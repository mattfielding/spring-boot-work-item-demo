package net.mattfielding.workitemdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkitemDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkitemDemoApplication.class, args);
	}

}
