package net.mattfielding.workitemdemo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import net.mattfielding.workitemdemo.util.ListUtil;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity(name = "bugs")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Bug extends WorkItem
{
    @Lob
    private String reproSteps;

    @Lob
    private String expectedBehaviour;

    @Lob
    private String actualBehaviour;

    private boolean fixed;

    @ManyToMany(mappedBy = "bugs")
    @JsonIgnore
    private List<TestCase> affectedTestCases;


    public String getReproSteps() {
        return reproSteps;
    }

    public void setReproSteps(String reproSteps) {
        this.reproSteps = reproSteps;
    }

    public String getExpectedBehaviour() {
        return expectedBehaviour;
    }

    public void setExpectedBehaviour(String expectedBehaviour) {
        this.expectedBehaviour = expectedBehaviour;
    }

    public String getActualBehaviour() {
        return actualBehaviour;
    }

    public void setActualBehaviour(String actualBehaviour) {
        this.actualBehaviour = actualBehaviour;
    }

    public boolean isFixed() {
        return fixed;
    }

    public void setFixed(boolean fixed) {
        this.fixed = fixed;
    }

    public List<TestCase> getAffectedTestCases() {
        return affectedTestCases;
    }

    public void setAffectedTestCases(List<TestCase> affectedTestCases) {
        this.affectedTestCases = affectedTestCases;
    }

    public int getAffectedTestCaseCount()
    {
        return ListUtil.getSize(affectedTestCases);
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.reflectionToString(this);
    }
}
