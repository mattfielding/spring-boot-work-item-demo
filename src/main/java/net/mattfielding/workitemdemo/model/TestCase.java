package net.mattfielding.workitemdemo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@Entity(name = "testcases")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class TestCase extends WorkItem {
    @Lob
    private String summary;

    @Lob
    private String steps;

    @ManyToMany
    @JoinTable(
            name = "testcase_bugs",
            joinColumns = @JoinColumn(name = "testcase_id"),
            inverseJoinColumns = @JoinColumn(name = "bug_id")
    )
    @JsonIgnore
    private List<Bug> bugs;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<Bug> getBugs() {
        return bugs;
    }

    public void setBugs(List<Bug> bugs) {
        this.bugs = bugs;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }
}
