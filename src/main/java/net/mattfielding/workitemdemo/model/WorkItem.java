package net.mattfielding.workitemdemo.model;

import javax.persistence.*;

//@MappedSuperclass
@Entity(name = "workitems")
@Inheritance(strategy = InheritanceType.JOINED)
public class WorkItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "WorkItem{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
