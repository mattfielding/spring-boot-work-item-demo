package net.mattfielding.workitemdemo.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class ListUtil {
    private ListUtil() {}

    public static <T> List<T> getListOrEmpty(List<T> list)
    {
        return list != null ? list : Collections.emptyList();
    }

    public static <T> int getSize(List<T> list)
    {
        return list != null ? list.size() : 0;
    }

    public static <T> String asString(List<T> list)
    {
        return asString(list.stream());
    }

    public static <T> String asString(T[] array)
    {
        return asString(Arrays.stream(array));
    }

    private static <T> String asString(Stream<T> stream)
    {
        return stream.map(Object::toString).collect(Collectors.joining(", ", "[", "]"));
    }
}
