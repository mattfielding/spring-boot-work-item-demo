package net.mattfielding.workitemdemo.controller;

import net.mattfielding.workitemdemo.aspect.audit.Audit;
import net.mattfielding.workitemdemo.aspect.logging.LoggedMethod;
import net.mattfielding.workitemdemo.model.Bug;
import net.mattfielding.workitemdemo.repository.BugRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v1/bugs")
public class BugController {
    @Autowired
    private BugRepository bugRepository;

    @GetMapping
    @LoggedMethod
    public List<Bug> list()
    {
        return bugRepository.findAll();
    }

    @GetMapping
    @RequestMapping("{id}")
    @LoggedMethod
    @Audit
    public Bug get(@PathVariable Long id)
    {
        var bug = bugRepository.findById(id);
        if (bug.isPresent())
        {
            return bug.get();
        }
        else
        {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Bug %d does not exist".formatted(id));
        }
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @LoggedMethod
    @Audit
    public Bug create(@RequestBody final Bug bug)
    {
        return bugRepository.saveAndFlush(bug);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @LoggedMethod
    @Audit
    public void delete(@PathVariable Long id)
    {
        bugRepository.deleteById(id);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    @LoggedMethod
    @Audit
    public Bug update(@PathVariable Long id, @RequestBody Bug bug)
    {
        Bug existingBug = bugRepository.getById(id);
        BeanUtils.copyProperties(bug, existingBug, "id", "affectedTestCases");
        return bugRepository.saveAndFlush(existingBug);
    }
}
