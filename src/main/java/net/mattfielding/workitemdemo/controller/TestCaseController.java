package net.mattfielding.workitemdemo.controller;

import net.mattfielding.workitemdemo.aspect.audit.Audit;
import net.mattfielding.workitemdemo.model.Bug;
import net.mattfielding.workitemdemo.model.TestCase;
import net.mattfielding.workitemdemo.repository.BugRepository;
import net.mattfielding.workitemdemo.repository.TestCaseRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/testcases")
public class TestCaseController {
    @Autowired
    protected TestCaseRepository repository;

    @Autowired
    private BugRepository bugRepository;

    @GetMapping
    public List<TestCase> list()
    {
        return repository.findAll();
    }

    @GetMapping
    @RequestMapping("{id}")
    @Audit
    public TestCase get(@PathVariable Long id)
    {
        return repository.findById(id).orElseThrow();
    }

    @PostMapping
    @Audit
    public TestCase create(@RequestBody TestCase newObject)
    {
        return repository.saveAndFlush(newObject);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @Audit
    public void delete(@PathVariable Long id)
    {
        repository.deleteById(id);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    @Audit
    public TestCase update(@PathVariable Long id, @RequestBody TestCase newObject)
    {
        TestCase existingObject = repository.getById(id);
        BeanUtils.copyProperties(newObject, existingObject, "id");
        return repository.saveAndFlush(existingObject);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PATCH)
    @Audit
    public TestCase updateBugLinks(@PathVariable Long id, @RequestBody List<Bug> newBugList)
    {
        TestCase existingObject = repository.getById(id);
        for (var bug : newBugList)
        {
            var possibleBug = bugRepository.findById(bug.getId());
            if (possibleBug.isEmpty())
            {
                continue;
            }
            var newBug = possibleBug.get();
            if (!existingObject.getBugs().contains(newBug))
            {
                existingObject.getBugs().add(newBug);
            }
        }
        return repository.saveAndFlush(existingObject);
    }
}
