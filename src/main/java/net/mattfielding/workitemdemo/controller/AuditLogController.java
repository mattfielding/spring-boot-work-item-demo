package net.mattfielding.workitemdemo.controller;

import net.mattfielding.workitemdemo.aspect.logging.LoggedMethod;
import net.mattfielding.workitemdemo.model.AuditLogEntry;
import net.mattfielding.workitemdemo.repository.AuditLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/audit")
public class AuditLogController {
    @Autowired
    private AuditLogRepository auditLog;

    @GetMapping
    @LoggedMethod
    public List<AuditLogEntry> list()
    {
        return auditLog.findByOrderByTimestampDesc();
    }
}
