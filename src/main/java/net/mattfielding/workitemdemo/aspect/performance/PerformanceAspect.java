package net.mattfielding.workitemdemo.aspect.performance;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.logging.Logger;

@Aspect
@Service
public class PerformanceAspect {

    private final Logger logger = Logger.getLogger(PerformanceAspect.class.getName());

    @Pointcut("execution(public * net.mattfielding.workitemdemo.controller.*.*(..))")
    public void performanceMeasuredMethods() {}

    @Around("performanceMeasuredMethods()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        var stopwatch = new StopWatch();
        stopwatch.start();
        try {
            return joinPoint.proceed();
        }
        finally {
            stopwatch.stop();
            logger.info("Method %s.%s took %d ms"
                    .formatted(
                            joinPoint.getSignature().getDeclaringTypeName(),
                            joinPoint.getSignature().getName(),
                            stopwatch.getTotalTimeMillis()
            ));
        }
    }
}
