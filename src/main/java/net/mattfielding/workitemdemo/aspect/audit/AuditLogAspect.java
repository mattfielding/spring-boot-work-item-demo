package net.mattfielding.workitemdemo.aspect.audit;

import net.mattfielding.workitemdemo.model.AuditLogEntry;
import net.mattfielding.workitemdemo.repository.AuditLogRepository;
import net.mattfielding.workitemdemo.util.ListUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.logging.Logger;

@Aspect
@Service
public class AuditLogAspect {
    private final Logger logger = Logger.getLogger(AuditLogAspect.class.getName());

    @Autowired
    private AuditLogRepository auditLog;

    @Pointcut("@annotation(net.mattfielding.workitemdemo.aspect.audit.Audit)")
    public void auditedMethods() {}

    @AfterReturning(value = "auditedMethods()", returning = "retVal")
    public void logMethodCall(JoinPoint joinPoint, Object retVal)
    {
        var auditLogEntry = constructAuditLogEntry(joinPoint);
        if (retVal != null) {
            auditLogEntry.setReturnData(retVal.toString());
        }

        try
        {
            auditLog.saveAndFlush(auditLogEntry);
        }
        catch (Throwable ex)
        {
            logger.severe("Unable to log message to audit log: " + ex);
        }
    }

    @AfterThrowing(value = "auditedMethods()", throwing = "throwable")
    public void logFailedMethodCall(JoinPoint joinPoint, Throwable throwable)
    {
        var auditLogEntry = constructAuditLogEntry(joinPoint);
        auditLogEntry.setReturnData("Failed due to " + throwable);

        try
        {
            auditLog.saveAndFlush(auditLogEntry);
        }
        catch (Throwable ex)
        {
            Logger.getLogger(this.getClass().getName()).severe("Unable to log message to audit log: " + ex);
        }
    }

    private AuditLogEntry constructAuditLogEntry(JoinPoint joinPoint)
    {
        var auditLogEntry = new AuditLogEntry();

        auditLogEntry.setTimestamp(Date.from(Instant.now()));
        auditLogEntry.setUser(null);

        auditLogEntry.setArea(joinPoint.getSignature().getDeclaringTypeName());
        auditLogEntry.setAction(joinPoint.getSignature().getName());
        auditLogEntry.setInputData(ListUtil.asString(joinPoint.getArgs()));

        return auditLogEntry;
    }
}
