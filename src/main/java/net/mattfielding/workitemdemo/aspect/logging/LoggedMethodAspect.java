package net.mattfielding.workitemdemo.aspect.logging;

import net.mattfielding.workitemdemo.util.ListUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Aspect
@Service
public class LoggedMethodAspect {
    @Pointcut("@annotation(net.mattfielding.workitemdemo.aspect.logging.LoggedMethod)")
    public void loggedMethods() {}

    @Before("loggedMethods()")
    public void before(JoinPoint joinPoint)
    {
        var message = "Calling method %s"
                .formatted(
                        getSignatureString(joinPoint)
                );
        getLogger(joinPoint).info(message);
    }

    @AfterReturning(value = "loggedMethods()", returning = "retVal")
    public void afterReturning(JoinPoint joinPoint, Object retVal)
    {
        var message = "%s returned %s"
                .formatted(
                        getSignatureString(joinPoint),
                        retVal
                );
        getLogger(joinPoint).info(message);
    }

    @AfterThrowing(value = "loggedMethods()", throwing = "ex")
    public void afterThrowing(JoinPoint joinPoint, Throwable ex)
    {
        var message = "%s threw %s with message: %s"
                .formatted(
                        getSignatureString(joinPoint),
                        ex.getClass().getName(),
                        ex.getMessage()
                );
        getLogger(joinPoint).info(message);
    }

    private Logger getLogger(@SuppressWarnings("rawtypes") Class loggerClass)
    {
        return Logger.getLogger(loggerClass.getName());
    }

    private Logger getLogger(JoinPoint joinPoint)
    {
        return getLogger(joinPoint.getSignature().getDeclaringType());
    }

    private String getSignatureString(JoinPoint joinPoint)
    {
        return "%s (%s)".formatted(
                joinPoint.getSignature().getName(),
                ListUtil.asString(joinPoint.getArgs())
        );
    }
}
