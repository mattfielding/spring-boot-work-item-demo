package net.mattfielding.workitemdemo.web;

import net.mattfielding.workitemdemo.controller.BugController;
import net.mattfielding.workitemdemo.model.Bug;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/bugs")
public class BugViewController {
    private static final String BugListPage = "bugs/buglist";
    private static final String BugDetailsPage = "bugs/bugdetails";
    private static final String BugEditPage = "bugs/bugedit";

    @Autowired
    private BugController bugController;

    @GetMapping
    public String listBugs(Model model)
    {
        model.addAttribute("bugs", bugController.list());
        return BugListPage;
    }

    @GetMapping("/{id}")
    public String getBugDetails(Model model, @PathVariable Long id)
    {
        model.addAttribute("bug", bugController.get(id));
        return BugDetailsPage;
    }

    @GetMapping("/create")
    public String createBug(Model model)
    {
        model.addAttribute("bug", new Bug());
        model.addAttribute("isExistingBug", false);
        return BugEditPage;
    }

    @GetMapping("/edit/{id}")
    public String editBug(Model model, @PathVariable Long id)
    {
        model.addAttribute("bug", bugController.get(id));
        model.addAttribute("isExistingBug", true);
        return BugEditPage;
    }

    @PostMapping(value = "/saveBug")
    public String saveBug(@ModelAttribute Bug bug, BindingResult errors, Model model)
    {
        var savedBug = bug.getId() == null ? bugController.create(bug) : bugController.update(bug.getId(), bug);
        return "redirect:/bugs/" + savedBug.getId();
    }
}
