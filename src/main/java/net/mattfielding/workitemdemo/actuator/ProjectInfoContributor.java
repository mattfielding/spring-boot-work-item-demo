package net.mattfielding.workitemdemo.actuator;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

@Component
public class ProjectInfoContributor implements InfoContributor {
    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("project_name", "Spring Boot Work Item Demo")
                .withDetail("owned_by", "Matthew Fielding");
    }
}
