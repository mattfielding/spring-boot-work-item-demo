package net.mattfielding.workitemdemo.actuator.endpoint.readiness;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Endpoint(id = "readiness")
public class ReadinessEndpoint {
    private Readiness ready = Readiness.NOT_READY;

    @ReadOperation
    public String getReadiness()
    {
        return ready.name();
    }

    @EventListener(ApplicationReadyEvent.class)
    public void setReady()
    {
        ready = Readiness.READY;
    }
}
