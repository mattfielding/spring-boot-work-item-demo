package net.mattfielding.workitemdemo.actuator.endpoint.readiness;

public enum Readiness {
    READY,
    NOT_READY
}
