package net.mattfielding.workitemdemo.repository;

import net.mattfielding.workitemdemo.model.TestCase;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestCaseRepository extends JpaRepository<TestCase, Long> {
}
