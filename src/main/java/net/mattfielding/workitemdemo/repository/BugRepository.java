package net.mattfielding.workitemdemo.repository;

import net.mattfielding.workitemdemo.model.Bug;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BugRepository extends JpaRepository<Bug, Long> {

}
