package net.mattfielding.workitemdemo.repository;

import net.mattfielding.workitemdemo.model.AuditLogEntry;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AuditLogRepository extends JpaRepository<AuditLogEntry,Long> {
    List<AuditLogEntry> findByOrderByTimestampDesc();
}
